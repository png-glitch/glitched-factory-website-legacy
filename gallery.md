---
layout: page
title: Gallery
permalink: /gallery/
show_header: true
---
<div class='center'>
    Here some examples of images that can be created by the Glitched Factory.
    Those images have been created by the sampler therefore with the default values of all the glitch algorithms and represent only a tiny sample of the capability of the software.
    On this page the images are optimized for the website, click on them to see the original images.
</div>

## Input image
{% include gallery_image.html image="cave" name="input image" %}

## Brush
{% include gallery_image.html image="cave_brush_horizontal" name="Brush horizontal" %}
{% include gallery_image.html image="cave_brush_horizontal_inverted" name="Brush horizontal inverted" %}
{% include gallery_image.html image="cave_brush_vertical" name="Brush vertical" %}
{% include gallery_image.html image="cave_brush_vertical_inverted" name="Brush vertical inverted" %}

## Change byte
{% include gallery_image.html image="cave_change_byte" name="Change byte" %}

## Exchange
{% include gallery_image.html image="cave_exchange_average" name="Exchange average" %}
{% include gallery_image.html image="cave_exchange_none" name="Exchange none" %}
{% include gallery_image.html image="cave_exchange_paeth" name="Exchange paeth" %}
{% include gallery_image.html image="cave_exchange_sub" name="Exchange sub" %}
{% include gallery_image.html image="cave_exchange_up" name="Exchange up" %}

## Slim
{% include gallery_image.html image="cave_slim_down_to_up" name="Slim down to up" %}
{% include gallery_image.html image="cave_slim_up_to_down" name="Slim up to down" %}
{% include gallery_image.html image="cave_slim_left_to_right" name="Slim left to right" %}
{% include gallery_image.html image="cave_slim_right_to_left" name="Slim right to left" %}

## Transpose
{% include gallery_image.html image="cave_transpose_average" name="Transpose average" %}
{% include gallery_image.html image="cave_transpose_none" name="Transpose none" %}
{% include gallery_image.html image="cave_transpose_paeth" name="Transpose paeth" %}
{% include gallery_image.html image="cave_transpose_sub" name="Transpose sub" %}
{% include gallery_image.html image="cave_transpose_up" name="Transpose up" %}

## Wrong filter
{% include gallery_image.html image="cave_wrong_filter_average" name="Wrong filter average" %}
{% include gallery_image.html image="cave_wrong_filter_none" name="Wrong filter none" %}
{% include gallery_image.html image="cave_wrong_filter_paeth" name="Wrong filter paeth" %}
{% include gallery_image.html image="cave_wrong_filter_sub" name="Wrong filter sub" %}
{% include gallery_image.html image="cave_wrong_filter_up" name="Wrong filter up " %}
