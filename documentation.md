---
layout: page
title: Documentation
permalink: /documentation/
show_header: true
---
<div class='center'>
  Understand all the options of the GlitchedFactory and how to create complex glitchs that fit your expectations.
</div>

## Getting started
To start using this software [download]({% link download.md %}) the version that fits your OS and follow the installation wizard (this should be straight forward). At the end of the installation you can simply start the software and have fun.

## In depth documentation
The sofware is split in different tabs, each tab serve a different purpose :


{% include documentation_index.html %}
