---
layout: page
title: About
permalink: /about/
show_header: false
show_footer: true
---

The Glitched Factory has been create by Florian G. with the help and feedbacks from [Vincent G.](https://gabrielli-av.net/).

The following sofware, language and libraries are use for the creation of it :
- [Ruby programming language](https://www.ruby-lang.org/en/)
- [Rust programming language](https://www.rust-lang.org/)
- [Shoes 3](http://walkabout.mvmanila.com/)
- [pnglitch](https://github.com/ucnv/pnglitch)
- [chunkyPNG](http://chunkypng.com/)

The icon has been created using glitch factory on an image created by [Paomedia](https://www.iconfinder.com/icons/285633/image_icon)

The title uses the [lack](http://velvetyne.fr/fonts/lack/) font.

This software is available under the [GNU General Public License V3](https://www.gnu.org/licenses/gpl-3.0.en.html) and has been written thanks to [Emacs](https://www.gnu.org/software/emacs/)

This website is created with [Jekyll](https://jekyllrb.com/) and hosted on [Framagit](https://framagit.org) an instance of [Gitlab](https://about.gitlab.com/)
