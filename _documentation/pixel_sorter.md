---
layout: page
title: Documentation - Pixel sorter
name: Pixel sorter
permalink: /documentation/pixel_sorter/
position: 3
---
The Pixel sorter screen help you to create a serie of pixel sorted images. The idea is to define the range of the first image and the last one. The first image of the folder will be sorted using the starting range and the last one the enfind range.

If the folder contain more than two image the images in between will be sorted using a range in between the two given range in evenly separated value. 

**Example :**
For a folder containing four images when the starting range is 0 (min) - 50 max) and the ending range is 30 (min) - 95 (max) the images will be sorted with those ranges :
  1. 0 (min) - 50 (max)
  2. 10 (min) - 65 (max)
  3. 20 (min) - 80 (max)
  4. 30 (min) - 95 (max)

## Input

For the pixel sorter the input can only be a folder, this folder should at least contains two images.

## Output

The output works the same of for the [glitcher screen]({% link _documentation/glitcher.md %}).

## Options
`Inverted sorting` : inverse the sorting (for example : light to dark become dark to light).

`Direction` : the direction of the sorting, either left to right either top to bottom.

`Sort by` :

  - `Hue` : the pixel with the bigger hue will be at the begining and the lower hue at the end.
  - `Saturation` : the pixel with the bigger saturation will be at the begining and the lower saturation at the end.
    
`Detection` :

 - `Smart` : create groups of colors and sort the content of each group separately.
 - `Brut` : sort the entire line or column.

`Detection type` : the attribute on wich the smart detection's groups are created.

`Lightness Ranges` : for smart detection using lightness range as a type. Determine which range need to be sorted (from 0 to 100).

### Starting / ending range
Define the range for the first image and the last image of the input folder

---

{% include pagination.html current_page=page %}
