---
layout: page
title: Documentation - Profiles
name: Profiles
permalink: /documentation/profiles/
position: 5
---
The profiles helps you to save a set of options that you want to easily reuse later. The creation of profile can be done on the **Glitcher screen** and on the **Sorting screen**, both those screen uses different profile which means that a profile created on the Glitcher screen can only be used on the Glitcher screen and the same goes for the Sorting screen.

The profiles **only save the options' section** wich means that the input and output are not saved.

To **create a new profile** you just need to set the options that you want, input a name for the profile and click on save. The name of the profile needs to be unique, but a Glitcher profile and a Sorting profile can have the same name.

When at least one profile is created a section to load it is visible, you just have to select the name of the profile that you want to use and click on load. 

Profile are persistent so they are saved when you close the Glitched Factory and can be used on the nexts sessions.

---

{% include pagination.html current_page=page %}
