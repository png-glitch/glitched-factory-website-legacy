---
layout: page
title: Documentation - Queue
name: Queue
permalink: /documentation/queue/
position: 4
---
The queue help you to quickly see what process is currently running. 

The queue is actualizing itself, the loading bar on the top of the screen show when it will be reloaded. If this feature bother you it can be deactivated in the settings.

If you want to reload the page you can simply click on the `Refresh` button

When nothing is running the queue only display the following message : 
`The queue is currently empty`

When one or more process is running an array display them. The array contain three columns : 

`Algorithm` : display the algorithm running, of you started a sampler the algorithm name is simply `Sampler`.

`Type` : 
  - `image` : the input is an image.
  - `directory` : the input is a folder.

`kill` : when clicked on the kill link of a proccess it will be stopped.

---

{% include pagination.html current_page=page %}
