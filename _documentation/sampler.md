---
layout: page
title: Documentation - Sampler
name: Sampler
permalink: /documentation/sampler/
position: 2
---

The sampler screen helps you to apply all algorithms with default values to on specific image. This is the perfect way to create a sample for one image to see which kind of aestetic you want to explore. 

The default values of each algoritms are the one predefines in the glitcher tab.

An example of a sampler output can be found on the [gallery page]({% link gallery.md %})
## Input
As for the [glitcher screen]({% link _documentation/glitcher.md %}) this section is for selecting the image you want to glitch. This time only one PNG image can be selected. 

## Output
This section again is very similar than the [glitcher screen]({% link _documentation/glitcher.md %}). 

The only difference is for the `Schema for output filename`, the sample will appened to the file name you choosed the algorithm and filter or direction used for creating the image. That way you can easilly know which output has been generated with which algorithm.

## Options
### Metadata
Select if metadata are written to the image.

### Algorithms
Select which algorithm to use in this sample, only checked algorithms will be used. 

---

{% include pagination.html current_page=page %}
