---
layout: page
title: Documentation - Glitcher
name: Glitcher
permalink: /documentation/glitcher/
position: 1
---


## Input
This section is for selecting the image(s) that you want to glitch.
**The Glitched Factory can only glitch PNG images.**

To **select an image to glitch** click on `Image to glitch` and inside to new windows browse to the specific image and open it. 

When selected the image will be shown in the form.

If you want to use the **same glitch on multiple image** you can create a directory containing all the PNG images and click on `Directory to glitch`, select the directory in the new windows and open it. 
Only the direct children are used, so if this folder contain other folder they will be ignored.

When selected the folder path will be displayed alongside the number of affected images.

## Output
This section is for specify the where to write the newly glitched image.
To specify the folder where you want to write your new image click on `Output directory` and select this directory.

When selected the output folder path will be displayed.

The `Schema for output filename` define the name of the image :
- if the input type is an image the output image will simply use the value of this field as name
- if the input type is a directory the output images will append the value of this field to their names

## Options
### Metadata
Select if metadata are written to the image, the following metadata are currently written : 
- `exif:Software` : Glitched Factory
- `exif:GlitchLibrary` : The library used for glitcing the image

If the input image already has a value for `exif:GlitchLibrary` the new one will be appended.

### Algorithms - Exchange
**Change the content of the image.**

`Range` :
define the intensity of the glitch, if the range is 1 the glitch occures only at one place. It only affect the exchange algorithm. When the range is 0 the glitch does not take the range into account.

`Random exchange` :
use a different value every time the glitcher is run, without it the same image is alway have the same glitched image.

`Seed` :
when not using randomness, the seed is a string of character that allow to recreate the same glitched image when using the glitcher many times

`Filter to use` :
the filter use for creating the glitched images, each filter create a different visual glitch.

### Algorithms - Transpose
**Split the image in four parts horizontally and exchange their places.**

`Transpose force` :

  - `Full` : all the sections are moved.
  - `Half` : only the two central sections are moved.

`Filter to use` : the filter use for creating the glitched images, each filter create a different visual glitch.

### Algorithms - Wrong filter
**Use a wrong png filter on the image.**

`Filter to use` : the filter use for creating the glitched images, each filter create a different visual glitch.

### Algorithms - Slim
**Take a pixel color and copy it the one or more pixels in a direction.**

`Slim direction` : the direction on which the pixel color is copied.

`Probability's area of effect` : 

  - `Global` : all color has the same probability to be slimed.
  - `Per color` : each color has a given probability to be slimed.

`Probability` : the probability in percent for each pixel to be slimed.

`Affected colors` : the colors that are slimed, when a color is unchecked a pixel with this color is never copied to the next one

### Algorithms - Brush
**Copy a pixel color in diagonal.**

`Brush direction` : the direction of the brush.

`Probability of brush` : the probability on which each pixel is brushed.

`Minimum of brushed pixel at a time` : the minimum amount of pixel brushed at a time.

`Maximum of brushed pixel at a time` : the maximum amount of pixel brushed at a time.

### Algorithms - Change bytes
**Change a bytes of an image.**

`Bytes` : the newly writen bytes will be the difference with the content of this attibute.

### Algorithms - Pixel sorting
**Sort the pixels of the image according to a given attribute.**

`Inverted sorting` : inverse the sorting (for example : light to dark become dark to light).

`Direction` : the direction of the sorting, either left to right either top to bottom.

`Sort by` :

  - `Hue` : the pixel with the bigger hue will be at the begining and the lower hue at the end.
  - `Saturation` : the pixel with the bigger saturation will be at the begining and the lower saturation at the end.
    
`Detection` :

 - `Smart` : create groups of colors and sort the content of each group separately.
 - `Brut` : sort the entire line or column.

`Detection type` : the attribute on wich the smart detection's groups are created.

`Lightness Ranges` : for smart detection using lightness range as a type. Determine which range need to be sorted (from 0 to 100).

---

{% include pagination.html current_page=page %}
