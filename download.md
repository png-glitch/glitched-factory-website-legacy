---
layout: page
title: Download
permalink: /download/
show_header: true
---
<div class='center'>
    You can find here the most recent version of the GlitchedFactory for your favorite OS : Linux, Macos or Windows.
</div>

<div class='center'>
{% include download_linux.html link="https://framagit.org/Radoteur/glitched_factory_website/-/raw/c2eb8a1445c6ce6317424642a04654847c1c087c/bin/GlitchedFactory-linux-0.29.deb" %}

{% include download_apple.html link="https://framagit.org/Radoteur/glitched_factory_website/-/raw/c2eb8a1445c6ce6317424642a04654847c1c087c/bin/GlitchedFactory-osx-0.29.dmg" %}

{% include download_windows.html link="https://framagit.org/Radoteur/glitched_factory_website/-/raw/c2eb8a1445c6ce6317424642a04654847c1c087c/bin/GlitchedFactory-windows-0.29.exe" %}
</div>
