---
layout: page
title: Releases
permalink: /releases/
show_header: false
show_footer: true
---

<div class='center'>
A list of releases for GlitchedFactory with a download link for all those versions.
</div>

<ul class="post-list">
    {% for post in site.posts %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
        <h3>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}" title="{{ post.title }}">{{ post.title }}</a>
        </h3>
      </li>
    {% endfor %}
  </ul>
